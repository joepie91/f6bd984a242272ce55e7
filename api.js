var Promise = require("bluebird");
var bhttp = Promise.promisifyAll(require("bhttp"));

// config encapsulates opensensors-api-key
// the only key config should have is "api-key"
module.exports = function(config) {
    var API_BASE_URL = "https://api.opensensors.io/v1/";
    var API_OPTIONS = {
        headers: {
            Accept: "application/json",
            Authorization: "api-key " + config["api-key"]
        }
    };
    
    // returns an array of message payloads from the API, augmented with timestamp
    function collectMessagesByDevice(client_id, params){
        var url = API_BASE_URL + "messages/device";
        if(!client_id){
            console.error("client-id is required");
            return Promise.resolve({});
        }
    
        url += "/" + client_id;
        var first = true;
        var results = [];
        var previousResultHasNext = null;
    
        promiseWhile(
            function(){ //predicate
                var ret = false;
                if(first || previousResultHasNext){
                    ret = true;
                }
                first = false;
                return ret;
            },
            function(){ // action
                previousResultHasNext = false;
                if(!params){
                    params = {};
                }
                return bhttp.post(url, params, API_HEADERS).then(function(response){
                    results.push(response);
                    previousResultHasNext = response.next;
                });
            }
        ).then(function(){
            console.log("Done");
        });
    }
    
    return {
        messages: {
            byDevice: collectMessagesByDevice,
            byTopic: function(topic, params){

            },
            byUser: function(user_id, params){

            }
        }
    };
};

var promiseWhile = function(condition, action) {
    var resolver = Promise.defer();

    var loop = function() {
        if (!condition()) return resolver.resolve();
        return Promise.cast(action())
            .then(loop)
            .catch(resolver.reject);
    };

    process.nextTick(loop);

    return resolver.promise;
};